=== Tainá ===

Contributors: wetah
Tags: custom-background, custom-logo, custom-menu, featured-images, threaded-comments, translation-ready

Requires at least: 4.5
Tested up to: 4.8
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: LICENSE

A new theme for exposing collections with Tainacan.

== Description ==

A modern theme for your digital expositions that works with [Tainacan](https://tainacan.org). Tainá is made for museums, libraries and galleries for sharing their stories with the World.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Tainá includes support for Tainacan and Infinite Scroll in Jetpack.

== Credits ==
* Mostly adapted from the original [Taincan Interface](https://wordpress.org/themes/tainacan-interface/) theme.
* Based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
