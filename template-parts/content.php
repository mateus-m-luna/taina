<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package taina
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php

	edit_post_link(
		sprintf(
			'<i class="tainacan-icon tainacan-icon-edit"></i>',
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( '<span class="screen-reader-text">Edit %s</span>', 'taina' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		),
		'<span class="edit-link">',
		'</span>'
	);

	?>

	<?php taina_post_thumbnail(); ?>

	<?php if ( is_home () || is_category() || is_archive() ) :
		?>
		<header class="entry-header">
			<?php

			if ( is_singular() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;

			?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php the_excerpt(''); ?>
		</div>

		<footer class="entry-footer">
			<?php taina_entry_footer(); ?>
		</footer><!-- .entry-footer -->
	<?php
	else : 
	?> <!-- If is not excerpt -->
		<header class="entry-header">
			<?php
			if ( is_singular() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;

			if ( 'post' === get_post_type() ) :
				?>
				<div class="entry-meta">
					<?php

					taina_posted_by();
					taina_posted_on();
					
					?>
				</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			
			<?php
			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'taina' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'taina' ),
				'after'  => '</div>',
			) );
			?>
		</div><!-- .entry-content -->

		<footer class="entry-footer">
			<?php taina_entry_footer(); ?>
		</footer><!-- .entry-footer -->
	<?php endif; ?> <!-- If not excerpt-->
</article><!-- #post-<?php the_ID(); ?> -->
