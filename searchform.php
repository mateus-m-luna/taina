<?php
/**
 * The searchbar for our theme
 *
 * This is the template that displays all default search forms rendered by get_search_form()
 *
 * @package taina
 */

?>
<form class="search-form" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="search-wrap">
        <label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'taina' ); ?></label>
        <input type="search" class="search-field" placeholder="<?php echo esc_attr( 'Search…', 'taina' ); ?>" name="s" id="search-input" value="<?php echo esc_attr( get_search_query() ); ?>" />
        <button type="submit" class="search-submit">
            <span class="screen-reader-text"><?php echo esc_attr( 'Search', 'taina' ); ?></span>
            <i class="tainacan-icon tainacan-icon-search"></i>
        </button>
    </div>
</form>