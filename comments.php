<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package taina
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
global $current_user;
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
		<h2 class="comments-title">
			<?php
			$taina_comment_count = get_comments_number();
			if ( '1' === $taina_comment_count ) {
				printf(
					/* translators: 1: title. */
					esc_html__( 'One comment', 'taina' ),
					'<span>' . get_the_title() . '</span>'
				);
			} else {
				printf( // WPCS: XSS OK.
					/* translators: 1: comment count number. */
					esc_html( _nx( '%1$s comment', '%1$s comments', $taina_comment_count, 'taina' ) ),
					number_format_i18n( $taina_comment_count ),
					'<span>' . get_the_title() . '</span>'
				);
			}
			?>
		</h2><!-- .comments-title -->

		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php
				$args = array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 120
				);
				// Use our custom walker if it's available
				if( class_exists( 'Taina_Walker_Comment' ) )
				{
					$args['walker'] = new Taina_Walker_Comment;
				}
				wp_list_comments($args);
			?>
		</ol><!-- .comment-list -->

		<?php the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'taina' ); ?></p>
			<?php
		endif;

	endif; // Check for have_comments().

	$user_link = sprintf( '<a href="%1$s">%2$s</a>', get_author_posts_url( $current_user->ID ), $current_user->display_name );
	$comment_args = array(
		'logged_in_as' => '',
		'title_reply' => '',
		'title_reply_before' => '',
		'title_reply_after' => '',
		'comment_field' => sprintf(
			'<div class="comment">
				<div class="comment-body">
					<div class="comment-meta">
						<div class="comment-author vcard">
							<img src="%1$s" class="avatar avatar-120 photo" width="120" height="120">
							<div class="comment-metadata"><b>%2$s</b></div>
						</div>
					</div>
					<label for="comment" class="screen-reader-text">%3$s</label>
					<textarea name="comment" id="comment" tabindex="1" required class="comment-textarea" placeholder="%3$s"></textarea>
				</div>
			</div>', 
			get_avatar_url( $current_user->ID, array('size' => 120,) ),
			$user_link,
			__('Type your comment here...', 'taina') 
		)
	);
	comment_form( $comment_args );
	?>

</div><!-- #comments -->
