<?php
/**
 * Tainá Theme Customizer
 *
 * @package taina
 */

/**
 * Add customizer support for theme settings
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function taina_customize_register( $wp_customize ) {
	// Add postMessage support for site title and description 
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	
	// Add footer settings section and its settings
	$wp_customize->add_section( 'taina_footer' , array(
		'title' => __( 'Footer', 'taina')
	) );
	$wp_customize->add_setting( 'taina_footer_social_links', array (
		'default' => true
	) );
	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize,
		'taina_footer_social_links',
		array(
			'label'	   => __( 'Show social links bar', 'taina' ),
			'section'  => 'taina_footer',
			'settings' => 'taina_footer_social_links',
			'type' 	   => 'checkbox'
	)) );

	// Refreshes
	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'taina_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'taina_customize_partial_blogdescription',
		) );
	}
}
add_action( 'customize_register', 'taina_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function taina_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function taina_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function taina_customize_footer_social_links() {

	if ( get_theme_mod( 'taina_footer_social_links', true ) ):?> 
		<div class="footer-social-links">
			
		</div>
	<?php 
	endif;
}
add_action( 'taina_customize_footer_social_links', 'taina_customize_footer_social_links' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function taina_customize_preview_js() {
	wp_enqueue_script( 'taina-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'taina_customize_preview_js' );
