Tainá
===

A modern theme for your digital expositions that works with [Tainacan](https://tainacan.org). Tainá is made for museums, libraries and galleries for sharing their stories with the World.
